iFeel Java Methods
==================
This is an easy to run iFeel methods.

Run
=====
Run the Main.java file, there is a test exemple there.

Original Method Implementation
===============================
You can download the original implementation at: https://bitbucket.org/matheusaraujo/sentimental-analysis-methods

Studies and References
======================
A complete review of our benchamrking datasets and methods perforamnces can be found at: "A Benchmark Comparison of State-of-the-Practice Sentiment Analysis Methods".


Please cite us:
===============
@inproceedings{gonccalves2013comparing,
  title={Comparing and combining sentiment analysis methods},
  author={Gon{\c{c}}alves, Pollyanna and Ara{\'u}jo, Matheus and Benevenuto, Fabr{\'\i}cio and Cha, Meeyoung},
  booktitle={Proceedings of the first ACM conference on Online social networks},
  pages={27--38},
  year={2013},
  organization={ACM}
}

@article{nunes2015benchmark,
  title={A Benchmark Comparison of State-of-the-Practice Sentiment Analysis Methods},
  author={Nunes Ribeiro, Filipe and Ara{\'u}jo, Matheus and Gon{\c{c}}alves, Pollyanna and Benevenuto, Fabr{\'\i}cio and Andr{\'e} Gon{\c{c}}alves, Marcos},
  journal={arXiv preprint arXiv:1512.01818},
  year={2015}
}

Sorry to be brief,  send an email to matheus.araujo@dcc.ufmg.br if any questions.