/**
 * Created by matheus on 12/29/15.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.keysolutions.ddpclient.DDPClient;
import com.keysolutions.ddpclient.DDPListener;
import com.lg.sentiment.Method;
import com.lg.sentiment.MethodCreator;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static org.ejml.ops.EjmlUnitTests.assertEquals;

public class iFeel {
    public static Boolean checkLineDoneAndUpdate(LinkedTreeMap<String, Object> lineobj, String lineId, Map<String, Method> methods, DDPClient ddp){
        if(Boolean.parseBoolean(lineobj.get("done").toString())){
            return true;
        }
        for (Map.Entry<String, Method> methodEntry : methods.entrySet()){
            if(lineobj.get(methodEntry.getKey()) == null){
                return false;
            }
        }


        Map<String,Object> options = new HashMap<String,Object>();
        Map<String,Object> setOptions = new HashMap<String,Object>();
        setOptions.put("done", true);
        options.put("$set", setOptions);
        ddp.collectionUpdate("linesCollection", lineId, options);

        return true;


    }
    public static Map convertObjectMap(Object obj){
        ArrayList<Object> list = new ArrayList<>();
        HashMap<String, String> map = null;
        try {
            map = (HashMap<String, String>) BeanUtils.describe(obj);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return map;
    }
    public static void generateMethods(Map<String, Method> methods) {
        MethodCreator methodCreator = new MethodCreator();
        methods.put("afinn", methodCreator.createMethod(1));
        methods.put("emolex", methodCreator.createMethod(2));
        methods.put("emoticons", methodCreator.createMethod(3));
        methods.put("emoticonds", methodCreator.createMethod(4));
        methods.put("happinessindex", methodCreator.createMethod(5));
        methods.put("opinionfinder", methodCreator.createMethod(6));
        methods.put("nrchashtag", methodCreator.createMethod(7));
        methods.put("opinionlexicon", methodCreator.createMethod(8));
        methods.put("panas", methodCreator.createMethod(9));
        methods.put("sann", methodCreator.createMethod(10));
        methods.put("sasa", methodCreator.createMethod(11));
        methods.put("senticnet", methodCreator.createMethod(12));
        methods.put("sentiment140", methodCreator.createMethod(13));
        methods.put("sentistrength", methodCreator.createMethod(14));
        methods.put("sentiwordnet", methodCreator.createMethod(15));
        methods.put("socal", methodCreator.createMethod(16));
        methods.put("stanford", methodCreator.createMethod(17));
        methods.put("umigon", methodCreator.createMethod(18));
        methods.put("vader", methodCreator.createMethod(19));

    }
    public static void main(String[] args) {

        String meteorIp = "localhost";
        String fileToAnalyse = null;
        Integer meteorPort = 3000;
        Boolean orphan = false;
        Map<String,Map<String,Boolean>> linesMethodDone = new HashedMap<>();
        Map<String,Boolean> lineMethodDoneFlags;
        Integer methodResult;

        for(int i = 0; i < args.length; i++) {
            if(args[i].contains("-p")){
                meteorPort = Integer.parseInt(args[i+1]);
            }
            if(args[i].contains("-h")){
                meteorIp = args[i+1];
            }
            if(args[i].contains("--orphan")){
                orphan = true;
            }
            if(args[i].contains("-f")){
                fileToAnalyse = args[i+1];
            }
        }
        System.out.printf("IP: " + meteorIp + "\n");
        System.out.printf("Port: " + meteorPort + "\n");
        System.out.printf("orphan: " + orphan + "\n");
        System.out.printf("fileToAnalyse: " + fileToAnalyse + "\n");

        try {
            DDPClient ddp = new DDPClient(meteorIp, meteorPort);
            // create DDP client observer
            DDPClient ddp2 = new DDPClient(meteorIp, meteorPort);
            DDPClient ddp3 = new DDPClient(meteorIp, meteorPort);
            iFeelObserver iFeelOBS = new iFeelObserver();
            iFeelObserver iFeelOBS2 = new iFeelObserver();
            iFeelObserver iFeelOBS3 = new iFeelObserver();
            ddp.addObserver(iFeelOBS);
            ddp2.addObserver(iFeelOBS2);
            ddp3.addObserver(iFeelOBS3);

            // add observer
            ddp.connect();
            ddp2.connect();
            Object[] subscribeArgs = new Object[1];
            Map<String,Object> subscribeOptions = new HashMap<String,Object>();
            subscribeOptions.put("file", fileToAnalyse);
            subscribeArgs[0] = subscribeOptions ;
            Map<String,Method> methods = new HashedMap<>();
            generateMethods(methods);


//          TODO: To remove
//           try{
//                if(orphan){
//                    ddp.subscribe("orphanLines", objects, iFeelOBS);
//                } else{
//                    ddp.subscribe("normalLines", objects, iFeelOBS);
//                }
//                ddp2.subscribe("linesAlmostDone", objects, iFeelOBS2);
//            }  catch (java.lang.NullPointerException e){
//                e.printStackTrace();
//            } catch (java.util.ConcurrentModificationException e){
//                e.printStackTrace();
//            }



            String lineId;
            LinkedTreeMap<String, Object> lineObj;
            if(fileToAnalyse != null){
                ddp.subscribe("fileLines", subscribeArgs, iFeelOBS);
            } else if(orphan){
                ddp.subscribe("orphanLines", subscribeArgs, iFeelOBS);
            } else{
                ddp.subscribe("normalLines", subscribeArgs, iFeelOBS);
            }

            while (true) {
                try{
                    Thread.sleep(2000);


                    if(ddp.getState() == DDPClient.CONNSTATE.Closed){
                        System.out.printf("Closed, reconnect again\n");
                        ddp = new DDPClient(meteorIp, meteorPort);
                        iFeelOBS = new iFeelObserver();
                        ddp.addObserver(iFeelOBS);

                        ddp.connect();
                        Thread.sleep(1000);
                        if(fileToAnalyse != null){
                            ddp.subscribe("fileLines", subscribeArgs, iFeelOBS);
                        } else if(orphan){
                            ddp.subscribe("orphanLines", subscribeArgs, iFeelOBS);
                        } else{
                            ddp.subscribe("normalLines", subscribeArgs, iFeelOBS);
                        }
                        Thread.sleep(500);
                    }

                    System.out.printf("State: " + ddp.getState().toString() + "\n");
                    System.out.printf("ErrorType: " +iFeelOBS.mErrorType + "\n");
                    System.out.printf("ErrorMsg: " +iFeelOBS.mErrorMsg + "\n");
                    System.out.printf("DdpState: " +iFeelOBS.mDdpState + "\n");

                    if(iFeelOBS.mErrorMsg != null && (iFeelOBS.mErrorMsg.contains("refused") || iFeelOBS.mErrorMsg.contains("error"))){
                        System.out.println("Refused or Error, force reconnect.");
                        ddp.disconnect();
                        Thread.sleep(500);
                    }



                    if (iFeelOBS.mDdpState == iFeelObserver.DDPSTATE.Connected && iFeelOBS.mReadySubscription != null && iFeelOBS.mCollections.containsKey("linesCollection") && iFeelOBS.mCollections.size() != 0) {
                        Set<HashMap.Entry<String, Object>> entriesSet = iFeelOBS.mCollections.get("linesCollection").entrySet();
                        //Converting to entriesSet to List
                        List<HashMap.Entry<String, Object>> entries = new ArrayList<HashMap.Entry<String, Object>>(entriesSet);
                        //Shuffle the list
                        Collections.shuffle(entries);
                        //Define limit to get it again
                        int LIMIT_PER_LOOP = (entries.size() > 10) ? 10 : entries.size();

                        for (int count = 0; count < LIMIT_PER_LOOP; count++) {
                            Map.Entry<String, Object> entry  = entries.get(count);

                            //get line id
                            lineId = entry.getKey();
                            //get line obj (methods)
                            lineObj = (LinkedTreeMap<String, Object>) entry.getValue();
                            //Define results to send
                            Object[] methodArgs = new Object[1];
                            Map<String, Object> params = new HashMap<String, Object>();
                            params.put("id", lineId);

                            //if line doens't have a text or is done continue to the next
                            if(checkLineDoneAndUpdate(lineObj,lineId,methods,ddp) || Boolean.parseBoolean(lineObj.get("done").toString())) continue;

                            Map<String,Object> options = new HashMap<String,Object>();
                            Map<String,Object> setOptions = new HashMap<String,Object>();
                            //Start to Analyse Line
                            String stripped = StringUtils.strip((String)lineObj.get("text"));
                            if(lineObj.get("text") == null || lineObj.get("text").toString().contains("null") || stripped.equals("")){
                                System.out.println("Set: " + lineId + " DONE"); 
                                setOptions.put("done", true);
                                options.put("$set", setOptions);
                                ddp.collectionUpdate("linesCollection", lineId, options);
                                continue;
                            }
                            System.out.println(entry.getKey() + "/" + entry.getValue() + "\n");
                            for (Map.Entry<String, Method> methodEntry : methods.entrySet()){
                                String name = methodEntry.getKey();

                                //Check if I already analysed it
                                Method method = methodEntry.getValue();
                                if(lineObj.get(name) != null){
                                    continue;
                                }
                                String text = lineObj.get("text").toString();
                                try {
                                    methodResult = method.analyseText(text.substring(0, Math.min(text.length(), 140)));
                                    setOptions.put(name, methodResult);
                                    options.put("$set", setOptions);
                                    ddp.collectionUpdate("linesCollection", lineId, options);
                                    options.clear();
                                    setOptions.clear();
                                }
                                catch (java.lang.IndexOutOfBoundsException e){
                                        //e.printStackTrace();
                                    System.out.println("Text: " + text);
                                    System.out.println("Unsupported Format, Setting Neutral");
                                    setOptions.put(name, 0);
                                    options.put("$set", setOptions);
                                    ddp.collectionUpdate("linesCollection", lineId, options);
                                    options.clear();
                                    setOptions.clear();
                                }


                            }
                            Thread.sleep(500);
                            checkLineDoneAndUpdate(lineObj,lineId,methods,ddp);

                        }
                    } else {
                        if (iFeelOBS.mCollections.size() == 0){
                            if(orphan){
                                System.out.printf("Empty orphanLines.\n");
                            } else {
                                System.out.printf("Empty normalLines.\n");
                            }

                        }
                    }
                } catch (InterruptedException e) {

                    e.printStackTrace();
                } catch (java.lang.NullPointerException e){
                    e.printStackTrace();
                } catch (java.util.ConcurrentModificationException e){
                    e.printStackTrace();
                }
            }


        } catch (java.net.URISyntaxException ex){
            System.out.printf("URI exception error.");
            ex.printStackTrace();
        }



//        Main.test02(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
    }
}
