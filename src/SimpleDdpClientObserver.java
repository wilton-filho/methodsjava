import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by matheus on 12/29/15.
 */
public class SimpleDdpClientObserver implements Observer {

    @Override
    public void update(Observable client, Object msg) {
        if (msg instanceof Map<?, ?>) {
            System.out.println("Received response: " + msg);

        }
    }

}