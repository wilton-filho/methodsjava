package com.lg.sentiment;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author jpaulo
 * sentence-level sentiment analysis method
 */
public abstract class Method {

	protected final int POSITIVE = 1;
	protected final int NEGATIVE = -1;
	protected final int NEUTRAL = 0;

	/**
	 * must load all lexicon needed to analyse sentences
	 */
	public abstract void loadDictionaries();

	/**
	 * @return the polarity
	 */
	public abstract int analyseText(String text);

	/**
	 * calls <code>analyseText</code> for each file's line and prints its polarity
	 */
	public void analyseFile(String filePath) {

		try {
			BufferedReader br = new BufferedReader(new FileReader(filePath));

			String line = br.readLine();
			while (line != null) {
				System.out.println(this.analyseText(line));
				line = br.readLine();
			}

			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
